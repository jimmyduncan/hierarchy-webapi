﻿using Hierachy.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text;

namespace Hierachy.Data
{
    public class HierachyContext : DbContext
    {
        public HierachyContext() : base("HierachyDb")
        {

        }

        public DbSet<Organism> Organisms { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Plant> Plants { get; set; }
        public DbSet<Bird> Birds { get; set; }
        public DbSet<Feline> Felines { get; set; }

        public DbSet<Interaction> Interactions { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Organism>()
                .HasMany(o => o.InteractionsAsOrigin)
                .WithRequired(i => i.OriginOrganism)
                .HasForeignKey(i => i.Origin_OrganismId);


            modelBuilder.Entity<Organism>()
                .HasMany(o => o.InteractionsAsTarget)
                .WithRequired(i => i.TargetOrganism)
                .HasForeignKey(i => i.Target_OrganismId);

            modelBuilder.Entity<Organism>()
                .HasMany(o => o.Comments)
                .WithRequired()
                .Map(m => m.ToTable("OrganismComments", "dbo"));
        }

    }
}
