﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hierachy.DataModels
{
    public class Interaction
    {
        public int InteractionId { get; set; }
        public int Origin_OrganismId { get; set; }
        public int Target_OrganismId { get; set; }
        public int InteractionType_ITPCodeId { get; set; }

        public ICollection<Comment> Comments { get; set; }
        public Organism OriginOrganism { get; set; }
        public Organism TargetOrganism { get; set; }
    }
}
