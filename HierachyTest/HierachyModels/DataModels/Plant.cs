﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hierachy.DataModels
{
    public class Plant : Organism
    {
        public override string ShortName
        {
            get
            {
                return $"Plant-{OrganismId}";
            }
        }
    }
}
