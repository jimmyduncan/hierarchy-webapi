﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hierachy.DataModels
{
    public class Bird : Animal
    {
        public override string ShortName
        {
            get
            {
                return $"B{OrganismId:D4}_{Name.Substring(0, 5)}";
            }
        }
    }
}
