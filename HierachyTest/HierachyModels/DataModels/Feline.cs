﻿using Hierachy.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace Hierachy.DataModels
{
    public class Feline : Animal
    {
        public int Coat_FCTCodeId { get; set; }

        public override string ShortName
        {
            get
            {
                return $"Feline-{OrganismId:D4}-{(Enums.Coats_FCT)Coat_FCTCodeId}";
            }
        }
    }
}
