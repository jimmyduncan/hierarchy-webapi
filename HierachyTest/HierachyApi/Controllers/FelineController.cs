﻿using Hierachy.Data;
using Hierachy.DataModels;
using Hierachy.Models;
using HierachyBusinessLogic;
using HierachyRepository;

namespace Hierachy.WebApi.Controllers
{
    public class FelineController : OrganismController<Feline, OrganismView>
    {
        public FelineController()
        {
            // Build the pipeline
            logic = new AnimalLogic<Feline, OrganismView>(
                new BaseRepository<Feline, int>(
                    new HierachyContext()
                )
            );
        }
    }
}