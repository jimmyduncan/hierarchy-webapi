﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Hierachy.DataModels;
using Hierachy.Models;
using HierachyBusinessLogic;

namespace Hierachy.WebApi.Controllers
{
    public abstract class OrganismController<TOrganism, TView> : ApiController
        where TView : OrganismView
        where TOrganism : Organism
    {
        protected IOrganismLogic<TOrganism, TView> logic;

        [HttpPost]
        public TView Create(TView organism)
        {
            TView result = logic.Create(organism);
            return result;
        }

        [HttpGet]
        public IEnumerable<TView> GetAll()
        {
            IEnumerable<TView> result = logic.GetAll();
            return result;
        }
    }
}