﻿using Hierachy.Data;
using Hierachy.DataModels;
using Hierachy.Models;
using HierachyBusinessLogic;
using HierachyRepository;

namespace Hierachy.WebApi.Controllers
{
    public class BirdController : OrganismController<Bird, OrganismView>
    {
        public BirdController()
        {
            // Build the pipeline
            logic = new AnimalLogic<Bird, OrganismView>(
                new BaseRepository<Bird, int>(
                    new HierachyContext()
                )
            );
        }
    }
}