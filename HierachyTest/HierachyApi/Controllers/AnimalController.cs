﻿using Hierachy.DataModels;
using Hierachy.Models;
using HierachyBusinessLogic;

namespace Hierachy.WebApi.Controllers
{
    public abstract class AnimalController<TAnimal, TView> : OrganismController<TAnimal, TView>
        where TView : OrganismView
        where TAnimal : Animal
    {
        
    }
}