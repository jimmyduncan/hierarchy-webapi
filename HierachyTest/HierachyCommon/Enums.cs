﻿using System;

namespace Hierachy.Common
{
    public class Enums
    {
        public enum Coats_FCT {
            Tabby = 1,
            Tuxedo = 2,
            White = 3,
            Black = 4
        }

        public enum OrganismType_OTP
        {
            Plant = 1,
            Feline = 2,
            Bird = 3
        }

        public enum InteractionType_ITP
        {
            Consumption = 1,
            Cohabitations = 2,
            Symbiosis = 3,
            Competition = 4
        }
    }
}
