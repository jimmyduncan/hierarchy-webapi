﻿using Hierachy.DataModels;
using Hierachy.Models;
using HierachyRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HierachyBusinessLogic
{
    public abstract class OrganismLogic<TOrganism, TView> : IOrganismLogic<TOrganism, TView>
        where TOrganism : Organism, new()
        where TView : OrganismView, new()
    {
        protected BaseRepository<TOrganism, int> repository;

        #region Organism

        public OrganismLogic(BaseRepository<TOrganism, int> repository)
        {
            this.repository = repository;
        }

        public TOrganism Get(int id)
        {
            return repository.Get(id);
        }

        public IEnumerable<TView> GetAll()
        {
            return repository.GetAll().Select(o => ConvertToView(o));
        }

        public abstract TView Update(TView organism);
        
        public TView Create(TView toCreate)
        {
            TOrganism data = ConvertToData(toCreate);
            repository.Create(data);
            repository.SaveChanges();
            return ConvertToView(data);
        }

        public void Delete(int id)
        {
            repository.Delete(id);
            repository.SaveChanges();
        }

        #endregion Organism

        #region Interaction

        #endregion Interaction

        #region Comment

        #endregion Comment

        protected TOrganism ConvertToData(TView view)
        {
            TOrganism data = new TOrganism();
            data.Description = view.Description;
            data.OrganismId = view.OrganismId;
            data.Name = view.Name;

            return data;
        }

        protected TView ConvertToView(TOrganism data)
        {
            TView view = new TView();
            view.Description = data.Description;
            view.OrganismId = data.OrganismId;
            view.Name = data.Name;
            view.ShortName = data.ShortName;

            return view;
        }
    }

    
}


