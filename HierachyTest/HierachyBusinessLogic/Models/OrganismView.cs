﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hierachy.Models
{
    public class OrganismView
    {
        public int OrganismId { get; set; }
        public int OrganismType_OTPCodeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
    }
}