﻿using Hierachy.DataModels;
using HierachyRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HierachyBusinessLogic
{
    public interface IOrganismLogic<TOrganism, TView>
        where TOrganism : Organism
    {
        TOrganism Get(int id);

        IEnumerable<TView> GetAll();

        TView Update(TView organism);

        TView Create(TView create);

        void Delete(int id);
    }
}
