﻿using Hierachy.DataModels;
using Hierachy.Models;
using HierachyRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HierachyBusinessLogic
{
    public class AnimalLogic<TAnimal, TView> : OrganismLogic<TAnimal, TView>, IOrganismLogic<TAnimal, TView>
        where TAnimal : Animal, new()
        where TView : OrganismView, new()
    {
        public AnimalLogic(BaseRepository<TAnimal, int> repository) : base(repository)
        {
        }
        
        public override TView Update(TView organism)
        {
            throw new NotImplementedException();
        }
    }
}


