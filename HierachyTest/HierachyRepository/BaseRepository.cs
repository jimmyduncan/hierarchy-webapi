﻿using Hierachy.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HierachyRepository
{
    public class BaseRepository<T, TKey>
        where T : class
    {
        private DbContext context;

        public BaseRepository(HierachyContext context)
        {
            this.context = context;
        }

        public T Get(TKey key)
        {
            return GetQuery().Find(key);
        }

        public List<T> GetAll()
        {
            return GetQuery().ToList();
        }

        public void Create(T model)
        {
            context.Set<T>().Add(model);
        }

        public void Delete(TKey key)
        {
            T model = Get(key);
        }

        public void SaveChanges()
        {
            context.SaveChanges();
        }

        protected DbSet<T> GetQuery()
        {
            return context.Set<T>();
        }
    }
}
