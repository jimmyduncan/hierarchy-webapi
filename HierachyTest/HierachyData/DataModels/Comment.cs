﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Hierachy.DataModels
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }
        string Text { get; set; }
    }
}
