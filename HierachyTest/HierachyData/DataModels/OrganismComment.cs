﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hierachy.DataModels
{
    public class OrganismComment : Comment
    {
        public int OrganismId { get; set; }
        public Organism Organism { get; set; }
    }
}
