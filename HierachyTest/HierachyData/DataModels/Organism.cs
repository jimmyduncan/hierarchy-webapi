﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Hierachy.DataModels
{
    public abstract class Organism
    {
        public Organism()
        {
        }

        [Key]
        public int OrganismId { get; set; }

        public int OrganismType_OTPCodeId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        
        [NotMapped]
        public abstract string ShortName { get; }

        public ICollection<Interaction> InteractionsAsOrigin { get; set; }
        public ICollection<Interaction> InteractionsAsTarget { get; set; }
        public ICollection<OrganismComment> OrganismComments { get; set; }
    }
}
