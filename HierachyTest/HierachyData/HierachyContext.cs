﻿using Hierachy.DataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Text;

namespace Hierachy.Data
{
    public class HierachyContext : DbContext
    {
        public HierachyContext() : base("HierachyDb")
        {

        }

        public DbSet<Organism> Organisms { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Plant> Plants { get; set; }
        public DbSet<Bird> Birds { get; set; }
        public DbSet<Feline> Felines { get; set; }

        public DbSet<Interaction> Interactions { get; set; }
        public DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Entity<Organism>().ToTable("Organism");
            modelBuilder.Entity<Plant>().ToTable("Plant");
            modelBuilder.Entity<Animal>().ToTable("Animal");
            modelBuilder.Entity<Bird>().ToTable("Bird");
            modelBuilder.Entity<Feline>().ToTable("Feline");

            modelBuilder.Entity<Interaction>().ToTable("Interaction");

            modelBuilder.Entity<Comment>().ToTable("Comment");
            modelBuilder.Entity<OrganismComment>().ToTable("OrganismComment");

            modelBuilder.Entity<Interaction>()
                .HasRequired(i => i.OriginOrganism)
                .WithMany(o => o.InteractionsAsOrigin)
                .HasForeignKey(i => i.Origin_OrganismId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Interaction>()
                .HasRequired(i => i.TargetOrganism)
                .WithMany(o => o.InteractionsAsTarget)
                .HasForeignKey(i => i.Target_OrganismId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Organism>()
                .HasMany(o => o.OrganismComments)
                .WithRequired(c => c.Organism)
                .HasForeignKey(c => c.OrganismId)
                .WillCascadeOnDelete(false);

        }

    }
}
