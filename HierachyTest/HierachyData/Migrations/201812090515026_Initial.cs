namespace HierachyData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Organism",
                c => new
                    {
                        OrganismId = c.Int(nullable: false, identity: true),
                        OrganismType_OTPCodeId = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.OrganismId);
            
            CreateTable(
                "dbo.Interaction",
                c => new
                    {
                        InteractionId = c.Int(nullable: false, identity: true),
                        Origin_OrganismId = c.Int(nullable: false),
                        Target_OrganismId = c.Int(nullable: false),
                        InteractionType_ITPCodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InteractionId)
                .ForeignKey("dbo.Organism", t => t.Origin_OrganismId)
                .ForeignKey("dbo.Organism", t => t.Target_OrganismId)
                .Index(t => t.Origin_OrganismId)
                .Index(t => t.Target_OrganismId);
            
            CreateTable(
                "dbo.Comment",
                c => new
                    {
                        CommentId = c.Int(nullable: false, identity: true),
                        Interaction_InteractionId = c.Int(),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Interaction", t => t.Interaction_InteractionId)
                .Index(t => t.Interaction_InteractionId);
            
            CreateTable(
                "dbo.Animal",
                c => new
                    {
                        OrganismId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrganismId)
                .ForeignKey("dbo.Organism", t => t.OrganismId)
                .Index(t => t.OrganismId);
            
            CreateTable(
                "dbo.Bird",
                c => new
                    {
                        OrganismId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrganismId)
                .ForeignKey("dbo.Animal", t => t.OrganismId)
                .Index(t => t.OrganismId);
            
            CreateTable(
                "dbo.Feline",
                c => new
                    {
                        OrganismId = c.Int(nullable: false),
                        Coat_FCTCodeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrganismId)
                .ForeignKey("dbo.Animal", t => t.OrganismId)
                .Index(t => t.OrganismId);
            
            CreateTable(
                "dbo.Plant",
                c => new
                    {
                        OrganismId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.OrganismId)
                .ForeignKey("dbo.Organism", t => t.OrganismId)
                .Index(t => t.OrganismId);
            
            CreateTable(
                "dbo.OrganismComment",
                c => new
                    {
                        CommentId = c.Int(nullable: false),
                        OrganismId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CommentId)
                .ForeignKey("dbo.Comment", t => t.CommentId)
                .ForeignKey("dbo.Organism", t => t.OrganismId)
                .Index(t => t.CommentId)
                .Index(t => t.OrganismId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.OrganismComment", "OrganismId", "dbo.Organism");
            DropForeignKey("dbo.OrganismComment", "CommentId", "dbo.Comment");
            DropForeignKey("dbo.Plant", "OrganismId", "dbo.Organism");
            DropForeignKey("dbo.Feline", "OrganismId", "dbo.Animal");
            DropForeignKey("dbo.Bird", "OrganismId", "dbo.Animal");
            DropForeignKey("dbo.Animal", "OrganismId", "dbo.Organism");
            DropForeignKey("dbo.Interaction", "Target_OrganismId", "dbo.Organism");
            DropForeignKey("dbo.Interaction", "Origin_OrganismId", "dbo.Organism");
            DropForeignKey("dbo.Comment", "Interaction_InteractionId", "dbo.Interaction");
            DropIndex("dbo.OrganismComment", new[] { "OrganismId" });
            DropIndex("dbo.OrganismComment", new[] { "CommentId" });
            DropIndex("dbo.Plant", new[] { "OrganismId" });
            DropIndex("dbo.Feline", new[] { "OrganismId" });
            DropIndex("dbo.Bird", new[] { "OrganismId" });
            DropIndex("dbo.Animal", new[] { "OrganismId" });
            DropIndex("dbo.Comment", new[] { "Interaction_InteractionId" });
            DropIndex("dbo.Interaction", new[] { "Target_OrganismId" });
            DropIndex("dbo.Interaction", new[] { "Origin_OrganismId" });
            DropTable("dbo.OrganismComment");
            DropTable("dbo.Plant");
            DropTable("dbo.Feline");
            DropTable("dbo.Bird");
            DropTable("dbo.Animal");
            DropTable("dbo.Comment");
            DropTable("dbo.Interaction");
            DropTable("dbo.Organism");
        }
    }
}
